﻿# -*- coding: utf-8 -*-
meta = [int(i) for i in input().rstrip().split(' ')]
line = []
for i in range(meta[1]):
    line.append(int(input().rstrip()))
max = sum(line[0:meta[0]])
now = max
for i in range(meta[1] - meta[0]):
    now = now + line[i + meta[0]] - line[i]
    if now > max:
        max = now
print(max)