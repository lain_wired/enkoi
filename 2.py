﻿# -*- coding: utf-8 -*-
num = int(input().rstrip())
result = 0
for i in range(num):
    data = [int(i) for i in input().rstrip().split(' ')]
    if data[0] > data[1]:
        result += (data[0] - data[1]) * data[2]
print(result)