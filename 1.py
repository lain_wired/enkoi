﻿# -*- coding: utf-8 -*-
num = int(input().rstrip())
result = 0
for i in range(num):
    result += int(input().rstrip())
print(result)